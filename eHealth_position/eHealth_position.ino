#include <eHealth.h>

void setup() {

  Serial.begin(9600);
  eHealth.initPositionSensor();
}

void loop() {

  Serial.print("Current position : ");
  uint8_t position = eHealth.getBodyPosition();
  eHealth.printPosition(position);

  Serial.print("\n");
  delay(1000); //	wait for a second.
}
