#include <eHealth.h>

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  float temperature = eHealth.getTemperature();

  Serial.print("Temperature (ºC): ");
  Serial.print(temperature, 2);
  Serial.println("");

  delay(1000);	// wait for a second
}
