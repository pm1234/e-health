
#include <PinChangeInt.h>
#include <eHealth.h>

int cont = 0;
unsigned long time = 0;

void setup() {
  Serial.begin(9600);
  eHealth.initPulsioximeter();

  //Attach the inttruptions for using the pulsioximeter.
  PCintPort::attachInterrupt(6, readPulsioximeter, RISING);
}

void loop() {
  //Data displayed by this function are saved to memory while calling the interrupt-dependent readPulsioximeter() function
  Serial.print("PRbpm : ");
  Serial.print(eHealth.getBPM());

  Serial.print("    %SPo2 : ");
  Serial.print(eHealth.getOxygenSaturation());

  Serial.print("\n");
  Serial.println("=============================");
  delay(1000);
}


//Include always this code when using the pulsioximeter sensor
//=========================================================================
void readPulsioximeter(){ //this method is called more less every 12-15 milliseconds (testes with millis() function)
  cont ++;
  if (cont == 50) { //Get only of one 50 measures to reduce the latency
    eHealth.readPulsioximeter();
    cont = 0;
  }
}
